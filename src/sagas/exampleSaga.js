import { takeLatest } from "redux-saga/effects";
import axios from "axios";

import {
  combineActionSaga,
  startAsyncAction,
  successAsyncAction,
  failAsyncAction,
} from "./combineActionSaga";
import {
  START_ASYNC_ACTION,
  SUCCESS_ASYNC_ACTION,
  FAIL_ASYNC_ACTION,
} from "../actions/actionTypes";

function* watchExampleSage() {
  yield takeLatest(
    "ASYNC_EXAMPLE_ACTION",
    combineActionSaga(
      startAsyncAction({ type: START_ASYNC_ACTION }),
      successAsyncAction({ type: SUCCESS_ASYNC_ACTION }),
      failAsyncAction({ type: FAIL_ASYNC_ACTION }),
      async (payload) => {
        const data = await axios.get(
          "https://jsonplaceholder.typicode.com/todos/1"
        );
        return data;
      }
    )
  );
}

export const exampleSaga = [watchExampleSage()];
