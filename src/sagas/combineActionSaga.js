import { put, call } from "redux-saga/effects";

export const startAsyncAction = ({ type, payload = {} }) =>
  function* (asyncPayload) {
    yield put({ type, payload: { ...payload, ...asyncPayload } });
  };
export const successAsyncAction = ({ type, payload = {} }) =>
  function* (asyncPayload) {
    yield put({ type, payload: { ...payload, ...asyncPayload } });
  };

export const failAsyncAction = ({ type, payload = {} }) =>
  function* (asyncPayload) {
    yield put({ type, payload: { ...payload, ...asyncPayload } });
  };

export const combineActionSaga = (
  startAsyncAction,
  successAsyncAction,
  failAsyncAction,
  process = () => {}
) =>
  function* () {
    if (startAsyncAction) {
      yield call(startAsyncAction);
    }

    if (successAsyncAction) {
      try {
        const response = yield call(process);
        yield call(successAsyncAction, response.data);
      } catch (error) {
        yield call(failAsyncAction);
      }
    }
  };
