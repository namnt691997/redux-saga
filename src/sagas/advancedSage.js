import { all, take, takeEvery, call } from "redux-saga/effects";
import { ADVANCED_ACTION } from "../actions/actionTypes";

function* doAdvanced() {
  console.log("ok");
}
function* nextAdvanced() {
  console.log("next");
}

function* advancedSage() {
  yield take(ADVANCED_ACTION);
  yield call(doAdvanced);
  yield take("NEXT_ADVANCED");
  yield call(nextAdvanced);
}

export default function* advancedSaga() {
  yield all([advancedSage()]);
}
