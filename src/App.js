import React from "react";
import "./App.css";
import { useDispatch } from "react-redux";

const App = () => {
  const dispatch = useDispatch();

  const handleSync = () => {
    dispatch({ type: "NEXT_ADVANCED" });
    dispatch({ type: "ADVANCED_ACTION" });
  };
  const handleAsync = () => {
    dispatch({ type: "ASYNC_EXAMPLE_ACTION" });
  };

  return (
    <div className="App">
      <button onClick={handleSync}>sync action</button>
      <button onClick={handleAsync}>async action</button>
    </div>
  );
};

export default App;
