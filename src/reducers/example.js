import {
  START_ASYNC_ACTION,
  SUCCESS_ASYNC_ACTION,
  FAIL_ASYNC_ACTION,
} from "../actions/actionTypes";

const initState = {};

const exampleReducer = (state = initState, action) => {
  switch (action.type) {
    case START_ASYNC_ACTION:
      return { ...state, ...action.payload };
    case SUCCESS_ASYNC_ACTION:
      return { ...state, ...action.payload };
    case FAIL_ASYNC_ACTION:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default exampleReducer;
